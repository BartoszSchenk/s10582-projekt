package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.*;

@RequestScoped

public class OsobaBuilder implements IEntityBuilder<Osoba>{

        @Override
        public Osoba build(ResultSet rs) {
                Osoba a = null;
                try
                {
                        a = new Osoba();
                        a.setName(rs.getString("name"));
                        a.setSurname(rs.getString("surname"));
                        a.setOddanie(rs.getDate("dataOdd"));
                        a.setWypozyczenie(rs.getDate("dataWyp"));
                }
                catch(Exception ex)
                {
                        ex.printStackTrace();
                }
                return a;
        }

}