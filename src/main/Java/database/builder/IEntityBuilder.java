package database.builder;

import java.sql.ResultSet;

import model.EntityItem;

public interface IEntityBuilder<E extends EntityItem> {

        public E build(ResultSet rs);
        
}