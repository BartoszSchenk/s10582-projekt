package database.builder;

import java.sql.ResultSet;
import javax.enterprise.context.RequestScoped;
import model.*;

@RequestScoped

public class AnimeBuilder implements IEntityBuilder<Anime>{

        @Override
        public Anime build(ResultSet rs) {
            Anime result = null;
            try{
                    
                    result = new Anime();
                    result.setTitle(rs.getString("title"));
                    result.setGenre(rs.getString("genre"));
                    result.setYear(rs.getInt("year"));
                    result.setEpisodes(rs.getInt("episodes"));
                    result.setAnimeType(AnimeType.valueOf(rs.getString("type")));
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

}