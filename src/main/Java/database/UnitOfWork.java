package database;

import java.util.Map;
import java.util.HashMap;
import dbifaces.*;
import model.*;

public class UnitOfWork implements IUnitOfWork{
	private Map<EntityItem, IUnitOfWorkRepository> added;
	private Map<EntityItem, IUnitOfWorkRepository> deleted;
	private Map<EntityItem, IUnitOfWorkRepository> updated;
	
	public UnitOfWork() {
		added   = new HashMap<EntityItem, IUnitOfWorkRepository>();
		deleted = new HashMap<EntityItem, IUnitOfWorkRepository>();
		updated = new HashMap<EntityItem, IUnitOfWorkRepository>();
	}
	
	@Override
	public void markAdded(EntityItem entity, IUnitOfWorkRepository repo){
		added.put(entity, repo);
	}
	
	@Override
	public void markDeleted(EntityItem entity, IUnitOfWorkRepository repo){
		deleted.put(entity, repo);
	}
	
	@Override
	public void markUpdated(EntityItem entity, IUnitOfWorkRepository repo){
		updated.put(entity, repo);
	}
	
	@Override
	public void commit(){
		for(EntityItem entity:added.keySet())
		{
			added.get(entity).persistAdded(entity);
		}
		for(EntityItem entity:deleted.keySet())
		{
			deleted.get(entity).persistDeleted(entity);
		}
		for(EntityItem entity:updated.keySet())
		{
			updated.get(entity).persistUpdated(entity);
		}
		added.clear();
		deleted.clear();
		updated.clear();
	}
	
}
