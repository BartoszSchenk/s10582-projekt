package database;

import dbifaces.*;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import model.EntityItem;
import dbifaces.IUnitOfWork;
import dbifaces.IUnitOfWorkRepository;

public class HibUnitOfWork implements IUnitOfWork{
	
	SessionFactory factory;
    Session session;
    private Map<EntityItem, IUnitOfWorkRepository> added;
    private Map<EntityItem, IUnitOfWorkRepository> deleted;
    private Map<EntityItem, IUnitOfWorkRepository> changed;
    
    
    public Session getSession() {
            if(session == null || !session.isOpen())
                    session=factory.openSession();
            return session;
    }

    public HibUnitOfWork() {
            super();
            added = new HashMap<EntityItem, IUnitOfWorkRepository>();
            deleted = new HashMap<EntityItem, IUnitOfWorkRepository>();
            changed = new HashMap<EntityItem, IUnitOfWorkRepository>();
            factory = new Configuration().configure().buildSessionFactory();
    }

    @Override
    public void markAdded(EntityItem ent, IUnitOfWorkRepository repo) {
            added.put(ent, repo);
            
    }

    @Override
    public void markDeleted(EntityItem ent, IUnitOfWorkRepository repo) {
            deleted.put(ent, repo);
            
    }

    @Override
    public void markUpdated(EntityItem ent, IUnitOfWorkRepository repo) {
            changed.put(ent, repo);
            
    }
    
    @Override
    public void commit() {
            try{
                    getSession().beginTransaction();
                    
                    for(EntityItem ent:added.keySet())
                    {
                            added.get(ent).persistAdded(ent);
                    }
                    for(EntityItem ent:changed.keySet())
                    {
                            changed.get(ent).persistUpdated(ent);
                    }
                    for(EntityItem ent:deleted.keySet())
                    {
                            deleted.get(ent).persistDeleted(ent);
                    }
                    
                    getSession().getTransaction().commit();
                    added.clear();
                    changed.clear();
                    deleted.clear();
            }catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            finally
            {
                    if(getSession().isOpen())
                    getSession().close();
            }
    }

}
