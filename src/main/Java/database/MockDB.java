package database;

import java.util.ArrayList;
import java.util.List;

import model.*;

public class MockDB {

        private ArrayList<EntityItem> entitylist = new ArrayList<EntityItem>();
        private int size;
        
        public ArrayList<EntityItem> getEntityList()
        {
                return entitylist;
        }
        
        public EntityItem get(int id)
        {
                return entitylist.get(id);
        }
        
        public void save(EntityItem item)
        {
                size++;
                item.setId(size);
                entitylist.add(item);
        }
        
        public void delete(EntityItem item)
        {
                entitylist.remove(item);
        }
        
        public <E extends EntityItem> List<E> getItemsByType(Class<E> c)
        {
                List<E> result = new ArrayList<E>();
                for(EntityItem item: entitylist)
                {
                        if(item.getClass().getName().equals(c.getName()))
                        {
                                result.add((E)item);
                        }
                }
                
                return result;
        }
        
}