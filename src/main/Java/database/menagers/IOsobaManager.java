package database.menagers;

import dbifaces.IManager;
import model.Anime;
import model.Osoba;

public interface IOsobaManager extends IManager<Osoba>{

	public void setAnimeList(Osoba o);
}
