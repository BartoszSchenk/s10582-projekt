package database.menagers.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import model.*;
import database.*;
import database.builder.*;
import database.menagers.*;
import dbifaces.*;

public class HibAnimeManager extends ManagerBase<Anime> implements IAnimeManager {
	
	Session session;
    
    public HibAnimeManager(HibUnitOfWork uow) {
            super(uow);
            this.session=uow.getSession();
    }

    @Override
    public void setOsoba(Anime a) {
            
    }

    @Override
    public Anime get(int id) {
            boolean openedNewSesson=false;
            if(!session.isOpen())
            {
                    session = ((HibUnitOfWork)uow).getSession();
                    openedNewSesson=true;
            }try{
            return (Anime)session.get(Anime.class, id);
            }finally
            {
                    if(openedNewSesson)
                            session.close();
            }
    }

    @Override
    public List<Anime> getAll(PageInfo request) {
            Query query = session.createQuery("from Books");
    query.setFirstResult(request.getPageIndex() * request.getPageSize());
    query.setMaxResults(request.getPageSize());
            return (List<Anime>)query.list();
    }

    @Override
    public void persistAdded(EntityItem ent) {
            Anime a = (Anime)ent;
            session.save(a);
    }

    @Override
    public void persistDeleted(EntityItem ent) {
            session.delete((Anime)ent);
            
    }

    @Override
    public void persistUpdated(EntityItem ent) {
            session.update((Anime)ent);
            
    }

}
