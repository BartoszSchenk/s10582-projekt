package database.menagers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.*;
import database.*;
import database.builder.*;
import database.menagers.*;
import dbifaces.*;


@RequestScoped

public class MysqlOsobaManager extends ManagerBase<Osoba> implements IOsobaManager{


    private IOsobaManager osobaMgr;
    private Connection connection;
    private String url = "jdbc:mysql://localhost/library?user=newuser&password=1qaz@WSX";
    
    private PreparedStatement getOsobaById;
    private PreparedStatement getAllOsoba;
    private PreparedStatement update;
    private PreparedStatement delete;
    private PreparedStatement insert;
    private IEntityBuilder<Osoba> builder;
    
    private Statement stmt;
    @Inject
    public MysqlOsobaManager(IUnitOfWork uow, IEntityBuilder<Osoba> builder) {
            
    	super(uow);
            
            this.builder=builder;
            
            try {

                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    connection = DriverManager.getConnection(url);
                    stmt =connection.createStatement();
                    
                    ResultSet rs = connection.getMetaData()
                                    .getTables(null,null,null,null);
                    
                    boolean exist = false;
                    while(rs.next())
                    {
                            if("Book".equalsIgnoreCase(rs.getString("TABLE_NAME")))
                            {
                                    exist=true;
                                    break;
                            }
                    }
                    
                    if(!exist)
                    {
                            
                    }
                    
                    getOsobaById = connection.prepareStatement(""
                                    + "SELECT * FROM author WHERE id=?");
                    getAllOsoba = connection.prepareStatement("SELECT * FROM author LIMIT ? ?");
                    insert = connection.prepareStatement(""
                                    + "INSERT INTO author(name,surname)"
                                    + "VALUES (?,?)");
                    delete = connection.prepareStatement(""
                                    + "DELETE FROM author WHERE id=?");
                    update = connection.prepareStatement("UPDATE author SET "
                                    + "(name,surname)=(?,?) "
                                    + "WHERE id=?");
                    
            } catch (Exception e) {
                    e.printStackTrace();
            }
            
    }

    @Override
    public void setAnimeList(Osoba o) {
    }
    
    @Override
    public Osoba get(int id) {
            Osoba result = null;
            try{
                    
                    getOsobaById.setInt(1, id);
                    ResultSet rs = getOsobaById.executeQuery();
                    while(rs.next())
                    {
                            result = builder.build(rs);
                    }
                    
            }catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

    @Override
    public List<Osoba> getAll(PageInfo request) {
            List<Osoba> anime = new ArrayList<Osoba>();
            
            try{
            int n = request.getPageIndex()*request.getPageSize();
            getAllOsoba.setInt(1, n);
            getAllOsoba.setInt(2, request.getPageSize());
            ResultSet rs = getAllOsoba.executeQuery();
            while(rs.next())
            {
                    Osoba result = builder.build(rs);
                    
                    anime.add(result);
            }
            
    }catch(Exception ex)
    {
            ex.printStackTrace();
    }
    return anime;
    }

    @Override
    public void persistAdded(EntityItem ent) {

            Osoba o = (Osoba)ent;
            try {
                    
                    insert.setString(1, o.getName());
                    insert.setString(2, o.getSurname());
                    insert.executeUpdate();
                    
            } catch (SQLException e) {
                    e.printStackTrace();
            }
            
    }

    @Override
    public void persistDeleted(EntityItem ent) {

            try {
                    delete.setInt(1, ent.getId());
                    delete.executeUpdate();
            } catch (SQLException e) {
                    e.printStackTrace();
            }
            
    }

    @Override
    public void persistUpdated(EntityItem ent) {
            Osoba o = (Osoba)ent;
            try{
                    update.setString(1, o.getName());
                    update.setString(2, o.getSurname());
                    update.setInt(4, ent.getId());
                    update.executeUpdate();
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            
    }
    
}
