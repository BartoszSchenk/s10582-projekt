package database.menagers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.*;
import database.*;
import database.builder.*;
import database.menagers.*;
import dbifaces.*;

@RequestScoped
public class MysqlAnimeManager extends ManagerBase<Anime> implements IAnimeManager{

	private IOsobaManager osobaMgr;
    private Connection connection;
    private String url = "jdbc:mysql://localhost/library?user=newuser&password=1qaz@WSX";
    
    private PreparedStatement getAnimeById;
    private PreparedStatement getAllAnimes;
    private PreparedStatement update;
    private PreparedStatement delete;
    private PreparedStatement insert;
    private IEntityBuilder<Anime> builder;
    private PreparedStatement getOsoba;
    
    private Statement stmt;
    
    
    @Inject
    public MysqlAnimeManager(IUnitOfWork uow, IEntityBuilder<Anime> builder, IOsobaManager osobaMgr) {
            
    	super(uow);
            
            this.builder=builder;
            this.osobaMgr=osobaMgr;
            
            try {

                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    connection = DriverManager.getConnection(url);
                    stmt =connection.createStatement();
                    
                    ResultSet rs = connection.getMetaData()
                                    .getTables(null,null,null,null);
                    
                    boolean exist = false;
                    while(rs.next())
                    {
                            if("Book".equalsIgnoreCase(rs.getString("TABLE_NAME")))
                            {
                                    exist=true;
                                    break;
                            }
                    }
                    
                    if(!exist)
                    {
                            stmt.executeUpdate(""
                                            + "CREATE TABLE Book("
                                            + "id int ,"
                                            + "publisher VARCHAR(50),"
                                            + "title VARCHAR(50),"
                                            + "year integer"
                                            + ")"
                                            + "");
                    }
                    
                    getAnimeById = connection.prepareStatement(""
                                    + "SELECT * FROM Book WHERE id=?");
                    getAllAnimes = connection.prepareStatement("SELECT * FROM Book LIMIT ? ?");
                    insert = connection.prepareStatement(""
                                    + "INSERT INTO Book(publisher,title,year)"
                                    + "VALUES (?,?,?)");
                    delete = connection.prepareStatement(""
                                    + "DELETE FROM Book WHERE id=?");
                    update = connection.prepareStatement("UPDATE Book SET "
                                    + "(publisher,title,year)=(?,?,?) "
                                    + "WHERE id=?");     
                    
            } catch (Exception e) {
                    e.printStackTrace();
            }
            
    }
    
    public void setOsoba(Anime a) {
}

    @Override
    public Anime get(int id) {
            Anime result = null;
            try{
                    
                    getAnimeById.setInt(1, id);
                    ResultSet rs = getAnimeById.executeQuery();
                    while(rs.next())
                    {
                            result = builder.build(rs);
                    }
                    
            }catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return result;
    }

    @Override
    public List<Anime> getAll(PageInfo mark) {
            List<Anime> anime = new ArrayList<Anime>();
            
                    try{
                    int n = mark.getPageIndex()*mark.getPageSize();
                    getAllAnimes.setInt(1, n);
                    getAllAnimes.setInt(2, mark.getPageSize());
                    ResultSet rs = getAllAnimes.executeQuery();
                    while(rs.next())
                    {
                            Anime result = builder.build(rs);
                            
                            anime.add(result);
                    }
                    
            }catch(Exception ex)
            {
                    ex.printStackTrace();
            }
            return anime;
    }

    @Override
    public void persistAdded(EntityItem ent) {
            
            Anime b = (Anime)ent;
            try {
                    
            		update.setString(1, b.getGenre());
            		update.setString(2, b.getTitle());
            		update.setInt(3, b.getYear());
                    insert.executeUpdate();
                    
            } catch (SQLException e) {
                    e.printStackTrace();
            }
    }

    @Override
    public void persistDeleted(EntityItem ent) {
            
            try {
                    delete.setInt(1, ent.getId());
                    delete.executeUpdate();
            } catch (SQLException e) {
                    e.printStackTrace();
            }
    }

    @Override
    public void persistUpdated(EntityItem ent) {
            Anime b = (Anime)ent;
            try{
                    update.setString(1, b.getGenre());
                    update.setString(2, b.getTitle());
                    update.setInt(3, b.getYear());
                    update.executeUpdate();
            }
            catch(Exception ex)
            {
                    ex.printStackTrace();
            }
    }
}
