package database.menagers.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import java.util.List;

import model.*;
import database.*;
import database.builder.*;
import database.menagers.*;
import dbifaces.*;

public class MockAnimeManager extends ManagerBase<Anime> implements IAnimeManager {

private MockDB db;

public MockAnimeManager(IUnitOfWork uow, MockDB db)
{
        this(uow);
        this.db=db;
}

private MockAnimeManager(IUnitOfWork uow) {
        super(uow);
}

@Override
public void setOsoba(Anime a) {

}

@Override
public Anime get(int id) {
        Anime book = new Anime();
        Anime a = (Anime)db.get(id-1);
        book.setId(a.getId());
        book.setGenre(a.getGenre());
        book.setTitle(a.getTitle());
        book.setYear(a.getYear());
        return book;
}

@Override
public List<Anime> getAll(PageInfo request) {
        return db.getItemsByType(Anime.class);
}

@Override
public void persistAdded(EntityItem ent) {
        db.save(ent);
        
}

@Override
public void persistDeleted(EntityItem ent) {
        db.delete(ent);
        
}

@Override
public void persistUpdated(EntityItem ent) {
        
}



}
