package model;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class Osoba extends EntityItem{
	
	private String name;
	private String surname;
	private Date dataWyp;
	private Date dataOdd;
	private List<Anime> Animelist = new ArrayList<Anime>();
	
	public List<Anime> getAnimelist(){
		return Animelist;
	}
	public void setAnimelist(List<Anime> Animelist){
		this.Animelist = Animelist;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name) {
        this.name = name;
	}
	
	public String getSurname(){
		return surname;
	}
	public void setSurname(String surname) {
        this.surname = surname;
	}
	
	public Date getWypozyczenie(){
		return dataWyp;
	}
	public void setWypozyczenie(Date dataWyp) {
        this.dataWyp = dataWyp;
	}
	
	public Date getOddanie(){
		return dataOdd;
	}
	public void setOddanie(Date dataOdd) {
        this.dataOdd = dataOdd;
	}
	
}
