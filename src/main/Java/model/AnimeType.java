package model;

public enum AnimeType {
	TVSerie, Movie, OVA, ONA, Special, PictureDrama, MusicVideo;
}
