package dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement

public class AnimeDto extends AnimeSummaryDto{

	private List<OsobaSummaryDto> osoba;

    public List<OsobaSummaryDto> getAuthors() {
            return osoba;
    }

    public void setAuthors(List<OsobaSummaryDto> osoba) {
            this.osoba = osoba;
    }
}
