package dto;

import model.AnimeType;
import model.Osoba;

public class AnimeSummaryDto {
	
	private String title;
	private String genre;
	private int year;
	private AnimeType type;
	private int episodes;
	private Osoba osoba;
	
	public Osoba getOsoba(){
		return osoba;		
	}
	public void setOsoba(Osoba osoba){
		this.osoba = osoba;
	}
	
	public String getTitle(){
		return title;
	}
	public void setTitle(String title) {
        this.title = title;
	}
	
	public String getGenre(){
		return genre;
	}
	public void setGenre(String genre) {
        this.genre = genre;
	}
	
	public int getYear(){
		return year;
	}
	public void setYear(int year) {
        this.year = year;
	}
	
	public void setAnimeType(AnimeType type){
		this.type = type;
	}
	
	public AnimeType getAnimeType(){
		return type;
	}
	
	public int getEpisodes(){
		return episodes;
	}
	public void setEpisodes(int episodes) {
        this.episodes = episodes;
	}
	
}
