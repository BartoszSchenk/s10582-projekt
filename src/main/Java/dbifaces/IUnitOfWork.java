package dbifaces;
import model.*;

public interface IUnitOfWork {
	
	public void markAdded(EntityItem entity, IUnitOfWorkRepository repo);
	public void markDeleted(EntityItem entity, IUnitOfWorkRepository repo);
	public void markUpdated(EntityItem entity, IUnitOfWorkRepository repo);
	public void commit();
}
