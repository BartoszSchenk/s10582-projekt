package dbifaces;
import java.util.List;
import model.*;
import database.PageInfo;

public interface IManager <E extends EntityItem> {
	public E get(int item);
	public List<E> getAll(PageInfo request);
	public void save(E object);
	public void delete(E object);
	public void update(E object);
	
}
