package dbifaces;
import model.*;

public interface IUnitOfWorkRepository {
	
	public void persistAdded(EntityItem entity);
	public void persistDeleted(EntityItem entity);
	public void persistUpdated(EntityItem entity);
}
